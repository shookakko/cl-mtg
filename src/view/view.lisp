(in-package :cl-mtg)


(defparameter *view-xml-location*
  (asdf:system-relative-pathname "cl-mtg" "src/view/mtg.xml"))

(defun build-view (&key empty-doom)
  "Return the view.xml file as a list to be evaluated."
  (when empty-doom
    (actor::empty-virtual-dom))
  (if (uiop:file-exists-p *view-xml-location*)
      (actor:handle-to-iup
       (actor:parse-xml-list
	(xmls:parse
	 (alexandria:read-file-into-string
	  *view-xml-location*))))
      (error "View XML file ~a not found."
	     *view-xml-location*)))


;; (defun show-view ()
;;   (iup:with-iup ()
;;     ;;(iup-scintilla:open)
;;     (sb-int:with-float-traps-masked
;; 	(:divide-by-zero :invalid)
;;       (let ((icon
;; 	      (iup-im:load-image
;; 	       (asdf:system-relative-pathname "iup" "examples/lispalien.ico"))))
;; 	(setf (iup:handle "elalien") icon)
;; 	(iup:show
;; 	 (iup:dialog
;; 	  (prog1 (eval (build-view)))))
;; 	(iup:main-loop)))))
