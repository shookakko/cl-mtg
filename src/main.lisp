(in-package :cl-mtg)

(defparameter *deck-example* (mtg-api:cards :where '(:colors "black") :where '(:cmc 4)))

(defparameter *images* nil)

(defmacro with-threads (&body body)
  `(let* ((cores (serapeum:count-cpus))
	  (lparallel:*kernel* (setf lparallel:*kernel*
				    (lparallel:make-kernel cores :name "custom-kernel"))))
     (unwind-protect
	  ,@body
       (lparallel:end-kernel :wait t))))

(defun download-cards ()
  (with-threads
    (lparallel:pmap
     'list
     (lambda (card)
       (let* ((name (substitute #\_ #\Space (mtg-api/card:name card)))
	      (url (mtg-api/card:image-url card))
	      (id (mtg-api/card:id card))
	      (location (format nil "/tmp/~a_~a" name id)))
	 (when (and name url id)
	   (unless (uiop:file-exists-p location)
	     (trivial-download:download url location)))))
     :size (length *deck-example*)
     *deck-example*)))

(actor:defcallback insertt () nil
  (create-containers)
  (loop for card in *deck-example*
	collect (let* ((name (substitute #\_ #\Space (mtg-api/card:name card)))
		       (url (mtg-api/card:image-url card))
		       (id (mtg-api/card:id card))
		       (location (format nil "/tmp/~a_~a" name id)))
		  (when (and name url id)
		    (when (uiop:file-exists-p location)
		      (and (setf (iup:handle name) (iup-im:load-image location))
			   (actor:insert-item (gethash "deck-list" actor:*virtual-dom*)
					      (actor:make-element
					       :id id
					       :type "label"
					       :attributes `(("IMAGE" ,name)))))))))
  (setf (iup:idle-action) nil))

(defun test-images ()
  (iup:with-iup ()
    (sb-int:with-float-traps-masked
	(:divide-by-zero :invalid)
      (iup:show
       (iup:dialog
	(prog1 
	    (eval (build-view)))))
      (setf (iup:idle-action) 'insertt)
      (iup:main-loop))))
