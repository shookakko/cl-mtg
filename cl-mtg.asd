(defsystem "cl-mtg"
  :version "0.0.1"
  :author ""
  :license "GNU GPLv3"
  :depends-on (:iup-im :actor
	       :mtg-api :trivial-download
	       :lparallel :serapeum)
  :components ((:module src
		:components
		((:file "package")
		 (:file "main")
		 (:module view
		  :components
		  ((:file "view"))))))
  :description "")


(defsystem "cl-mtg/tests"
  :author ""
  :license "GNU GPLv3"
  :depends-on (:cl-mtg :fiveam)
  :components ((:module tests
		:serial t
		:components
		((:file "package")
		 (:file "tests"))))
  :description "Test system for cl-mtg")
